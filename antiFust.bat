@ECHO off

:input
ECHO Input folder path or 'quit' to finish
SET init_path=%cd%
SET /p path=^



IF %path%==quit GOTO :exit
::get last character
set path_end=%path:~-1%

if %path_end%==\ (
	set path=%path:~0,-1%
)


IF NOT EXIST "%path%" (
	echo path doesn't exist
	GOTO :input
)

::get folder name from path
for %%F in ("%path%") do set "folder=%%~nxF"

::set path=%path:theFolder =%
echo Working on %folder%, please wait...
pushd %path%\..

echo %cd%

start attrib -S -H %folder%\* /S /D

echo Process completed

ECHO Do you want to unvail %folder% itself too {y / n - default} ?
set /p choice=^


if "%choice%"=="y" goto unvail
if "%choice%"=="yes" goto unvail
goto exit

:unvail
start attrib -S -H %folder% /S /D


pause
:exit
pushd %init_path%\